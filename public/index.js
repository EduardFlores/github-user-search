import { octocatUser } from './script/content.js';
import { theme } from './script/theme.js';

document.addEventListener('DOMContentLoaded', (even) => {
  octocatUser();
});
theme();
