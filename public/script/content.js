export const octocatUser = () => {
  // consulta a la api
  // let user = 'octocat';
  const searchUser = async (user = 'octocat') => {
    const link = `https://api.github.com/users/${user}`;

    const getApi = await fetch(link);
    if (getApi.status === 200) {
      const response = await getApi.json();
      document.querySelector('.profile__name').textContent = response.name;
      document.querySelector('.profile__avatar').src = response.avatar_url;
      document.querySelector(
        '.profile__user'
      ).textContent = `@${response.login}`;
      // date
      let fecha = new Date(response.created_at);
      var fechaFormateada = fecha.toLocaleDateString('en-US', {
        day: 'numeric',
        month: 'short',
        year: 'numeric',
      });

      let partesDeFecha = fechaFormateada.split(' ');

      let dia = partesDeFecha[1].replace(',', ' ');
      let mes = partesDeFecha[0];
      let anio = partesDeFecha[2];

      document.querySelector(
        '.profile__fecha'
      ).textContent = `Joined ${dia} ${mes} ${anio}`;
      //
      response.bio == null
        ? (document.querySelector('.profile__description').textContent =
            'This profile has no bio')
        : (document.querySelector('.profile__description').textContent =
            response.bio);

      document.getElementById('repo').textContent = response.public_repos;
      document.getElementById('follower').textContent = response.followers;
      document.getElementById('following').textContent = response.following;
      //
      response.location == null
        ? (document.getElementById('location').textContent = 'Not available')
        : (document.getElementById('location').textContent = response.location);
      //
      response.blog == ''
        ? (document.getElementById('website').textContent = 'Not available')
        : (document.getElementById('website').textContent = response.blog);
      //
      response.company == null
        ? (document.getElementById('company').textContent = 'Not available')
        : (document.getElementById('company').textContent = response.company);
      //
      response.twitter_username == null
        ? (document.getElementById('twitter').textContent = 'Not available')
        : (document.getElementById(
            'twitter'
          ).textContent = `@${response.twitter_username}`);
    }
  };

  searchUser();

  // evento
  const button = document.querySelector('.form__button');
  const input = document.querySelector('.form__input');
  const message = document.querySelector('.form__message');

  button.addEventListener('click', (event) => {
    // console.log(event);
    event.preventDefault();

    if (input.value === '') {
      console.log('no hay nombre');
      // codigo que muestre el span con el mensaje
      message.classList.add('show-message');
    } else {
      searchUser(input.value);
      message.classList.remove('show-message');
      // button.setAttribute('value', 'Search');
    }
  });
};

// TODO: si es 'not available' poner el icono y la letra en gris, acceder al icono y darle estilos grises y tambien al span
// TODO: crear un span dentro del form para 'no result' y verificar el status 404 y que tambien muestre 'no result'
