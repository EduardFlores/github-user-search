export const theme = () => {
  const _btnTheme = document.getElementById('bnt-theme');
  const _themeName = document.getElementById('theme-name');
  const _icon = document.getElementById('icon');
  const _body = document.querySelector('body');

  document.addEventListener('DOMContentLoaded', (even) => {
    load();
  });

  _btnTheme.addEventListener('click', (event) => {
    if (_themeName.textContent === 'DARK') {
      _themeName.textContent = 'LIGHT';
      _icon.src = 'assets/images/icon-sun.svg';
    } else {
      _themeName.textContent = 'DARK';
      _icon.src = 'assets/images/icon-moon.svg';
    }

    _body.classList.toggle('theme-dark');
    store(_body.classList.contains('theme-dark'));
  });

  const load = () => {
    const darkMode = localStorage.getItem('darkmode');
    if (!darkMode) {
      store('false');
    } else if (darkMode == 'true') {
      _body.classList.add('theme-dark');
    }
  };

  const store = (value) => {
    localStorage.setItem('darkmode', value);
  };
};
